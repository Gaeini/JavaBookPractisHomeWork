public class Emploee {

    private String name;
    private String family;
    private int    salary;
    private double tax = 0;


    Emploee(userInformation i) {

        this.name = i.name;
        this.family = i.family;
        this.salary = i.salary;
    }


    private double salaryScont() {

        if (this.salary < 500) {
            tax = 0;
        } else if (this.salary < 1000) {
            tax = (this.salary - 500) * 0.1;
        } else if (this.salary < 1500) {
            tax = ((this.salary - 500) * 0.1) + ((this.salary - 1000) * 0.2);
        }
        return this.salary - tax;
    }


    public void show() {
        System.out.print(this.name);
        System.out.print(" " + this.family);

    }


    public double getterSalaryScont() {

        return salaryScont();
    }

}
