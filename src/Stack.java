public class Stack {

    int   pointer;
    int[] stackArray = new int[10];


    public Stack() {
        pointer = -1;
    }


    public void push(int item) {

        if (pointer < 9) {
            stackArray[++pointer] = item;
        } else {
            System.out.println("Stack is Full");
        }
    }


    public int pop() {

        if (pointer > -1) {
            return stackArray[pointer--];

        } else {
            System.out.println("Stack is Empty");
            return -1;
        }
    }
}
