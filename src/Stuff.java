public class Stuff {

    private String name;
    private int    price;


    Stuff(userInformation i) {

        this.name = i.name;
        this.price = i.price;
    }


    public double priceScont(double scont) {
        return price * (1 - scont);
    }


    public double priceScont() {
        return priceScont(0.1);
    }


    public String Show() {
        return "Stuff Name is :" + name;
    }
}
